import django.contrib.auth.models
from django.db import models
from django.urls import reverse


class User(django.contrib.auth.models.AbstractUser):
    ssh_pubkey = models.TextField("SSH public key", help_text="Leave empty if not sure", blank=True)
    pgp_pubkey = models.TextField("PGP (mail) public key", help_text="Leave empty if not sure", blank=True)
    show_email = models.BooleanField("Show email publicly", default=False)
    show_keys = models.BooleanField("Show public keys publicly", default=False)

    def get_absolute_url(self):
        return reverse('user:profile', args=[self.pk])


class Subscriber(models.Model):
    email = models.EmailField("Email", unique=True)
    fullname = models.CharField("Full name", max_length=100)

    subscribed = models.BooleanField("Subscribe me to the mailing list",
                                     help_text="The mailing list is low-volume.",
                                     db_index=True)
    subscribed_projects = models.BooleanField("Subscribe me to project updates",
                                              db_index=True)

    investor = models.BooleanField("Am I an investor?", default=False)
    donor = models.BooleanField("Am I a donor?", default=False)
    nonprofit = models.BooleanField("Am I a nonprofit project owner?", default=False)

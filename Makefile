.PHONY: all clean

all: main/static/generated/abi.json main/static/generated/bytecode.dat

truffle/build/contracts/Nonprofit.json: truffle/contracts/Nonprofit.sol
	(cd truffle && truffle migrate)

main/static/generated/abi.json: truffle/build/contracts/Nonprofit.json
	python3 -c 'import sys, json; i = open("$<"); sys.stdout.write(json.dumps(json.load(i)["abi"]))' > $@

main/static/generated/bytecode.dat: truffle/build/contracts/Nonprofit.json
	python3 -c 'import sys, json; i = open("$<"); sys.stdout.write(json.load(i)["bytecode"])' > $@

clean:
	rm -f main/static/generated/abi.json main/static/generated/bytecode.dat
	rm -rf truffle/build

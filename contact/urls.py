from django.urls import path

import contact.views

app_name = 'contact'
urlpatterns = [
    path('contact', contact.views.ContactFormView.as_view(), name='contact'),
]

from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from django.views import View

from contact.forms import ContactForm


class ContactFormView(View):
    def get(self, request):
        form = ContactForm()
        return render(request, 'contact/contact.html', {'form': form})

    def post(self, request):
        form = ContactForm(request.POST)
        if form.is_valid():
            send_mail(form.cleaned_data['subject'], form.cleaned_data['text'], form.cleaned_data['from_email'],
                      settings.ADMIN_EMAILS)
            return render(request, 'contact/thanks.html', {'form': form})
        else:
            return render(request, 'contact/contact.html', {'form': form})

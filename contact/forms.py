from django import forms
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget


class ContactForm(forms.Form):
    required_css_class = 'required'

    from_email = forms.EmailField(label='Your email', required=True)
    subject = forms.CharField(label='Subject', max_length=996)  # RFC 2822
    text = forms.CharField(label='Text', widget=forms.Textarea, required=False)
    captcha = ReCaptchaField(widget=ReCaptchaWidget())

    # Check against newline injection attack
    def clean_subject(self):
        data = self.cleaned_data['subject']
        if data.find("\n") != -1 or data.find("\r") != -1:
            raise forms.ValidationError("Subject must be single-line.")
        return data

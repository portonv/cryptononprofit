# from django_countries import countries
# from django_countries.fields import LazyTypedChoiceField, CountryField, Country
from django.core.exceptions import ValidationError

from main.widget import EthereumAddressWidget, ContractTypeWidget
from project.base import STAGE_DEPLOY, STAGE_STORE, STAGE_EDIT
from project.fields import *
from project.models import Project, ProjectVersion, PaymentURL, ERC20Token, ProjectImage

# class ProjectForm(forms.ModelForm):
#     class Meta:
#         model = Project
#         fields = ['address']
from report.models import Withdrawal


class ProjectVersionForm(forms.ModelForm):
    required_css_class = 'required'

    # payment_urls = PaymentURLsField()

    class Meta:
        model = ProjectVersion
        fields = ['bitcoin_address',
                  # 'payment_urls',
                  'project_image',
                  'image',
                  'title',
                  'description',
                  'money_usage',
                  'allow_advertisement',
                  'allow_press_releases',
                  'url',
                  'ownership',
                  'country',
                  'organization_name',
                  'organization_number']
        widgets = {
            'description': forms.Textarea(),
            'money_usage': forms.Textarea(),
            'ownership': forms.RadioSelect(),
        }
        labels = {
            'allow_press_releases': "... but allow to use money for press releases",
        }

    image = forms.ImageField(required=False)

    # field_order = ['bitcoin_address',
    #                'payment_urls']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['ownership'].choices = self.fields['ownership'].choices[1:]
        # self.fields['country'].required = False

        # [] is not a QuerySet but it's expected to work
        payment_urls = kwargs['instance'].payment_urls if kwargs.get('instance') else []
        if not isinstance(payment_urls, list):  # TODO: hack (necessary?)
            payment_urls = payment_urls.all().values('url', 'title')
        self.fields['payment_urls'] = PaymentURLsField(
            initial=payment_urls,
            required=False,
            label="Additional payment URLs",
            help_text="For example <code>https://paypal.me/...</code> or link to your fundraiser at another site")
        # payment_urls = kwargs.pop("payment_urls", None)
        # self.field_order = ['bitcoin_address',
        #                     'payment_urls']
        if 'image' in self.files:
            project_image = ProjectImage.objects.create(image=self.files['image'])
            self.data = self.data.copy()
            self.data.update(project_image=project_image.pk)
            del self.files['image']  # The file is already read from the data stream.
        self.fields['project_image'].widget = forms.HiddenInput()
        self.fields['project_image'].required = False
        self.order_fields(['bitcoin_address',
                           'payment_urls'])

    def clean(self):
        cleaned_data = super().clean()
        self.instance.payment_urls_data = cleaned_data['payment_urls']

    def save(self, commit=True):
        model = super().save(commit)
        if 'image' in self.files:
            model.project_image = ProjectImage.objects.create(image=self.files['image'])
        return model

    # class ProjectOwnerForm(forms.ModelForm):


#     required_css_class = 'required'
#
#     class Meta:
#         model = ProjectVersion
#         fields = ['owner']


class ProjectContractSymbolForm(forms.ModelForm):
    required_css_class = 'required'

    CREATE_NO = 0
    CREATE_YES = 1
    CREATE_USE_EXISTING = 2

    WALLET_TYPE_REGULAR = -1
    WALLET_TYPE_UNKNOWN_CONTRACT = 0
    WALLET_TYPE_QUADRATIC_CONTRACT = 1

    class Meta:
        model = ERC20Token
        fields = ['owner', 'address', 'contract_symbol', 'contract_name', 'rules']

    def __init__(self, *args, create=False, stage=None, create_contract=None, **kwargs):
        """
        `create`: create vs update.

        `create_contract` means to show the intermediate dimmed form which will execute contract creation.
        """

        super().__init__(*args, **kwargs)

        self.fields['stage'] = forms.ChoiceField(widget=forms.HiddenInput(),
                                                 choices=((STAGE_EDIT, ''),
                                                          (STAGE_DEPLOY, ''),
                                                          (STAGE_STORE, '')))

        self.fields['create_contract'] = forms.ChoiceField(
            choices=((self.CREATE_NO, "Don't create" if create else "Don't change"),
                     (self.CREATE_YES, "Create contract (requires to spend some Ether)"),
                     (self.CREATE_USE_EXISTING, "Connect to existing contract or wallet")),
            label="Smart contract or Ether wallet",
            help_text="<p style='color:red'>You may be subject to certain legal restrictions disallowing to create such a contract, especially if you are from the US. Consult your investment laws.</p>"
                      "<p style='color:red'>Moreover, your contract won't be shown to the US customers.</p>"
                      "<p>You are recommended to create a contract for significant projects. " +
                      "Don't create a contract more than once for the same project unless absolutely sure you need it.</p>" +
                      "<p>You can buy Ether (or BitCoin) <a target='_blank' href='https://go.coinmama.com/visit/?bta=57420&nci=5360'>here</a>.</p>",
            initial=self.CREATE_NO,
            required=True,
            widget=forms.RadioSelect(
                attrs={'onchange': "create_contract_change()",
                       'class': "my-radio-widget"}))

        self.initial['stage'] = stage
        self.initial['create_contract'] = create_contract or 0
        self.make_ownerfield(stage)

        self.fields['rules'] = forms.ChoiceField(choices=((self.WALLET_TYPE_REGULAR, "Wallet"),
                                                          (self.WALLET_TYPE_UNKNOWN_CONTRACT, "Unknown contract"),
                                                          (self.WALLET_TYPE_QUADRATIC_CONTRACT, "Quadratic contract")),
                                                 widget=ContractTypeWidget(attrs={'class': "my-radio-widget"}),
                                                 initial=self.WALLET_TYPE_REGULAR)

        self.fields['address'].widget = EthereumAddressWidget()

        for i in 'contract_symbol', 'contract_name':
            self.fields[i].widget.attrs['disabled'] = create_contract != str(self.CREATE_YES)
            self.fields[i].required = False

        self.fields['address'].widget.attrs['disabled'] = \
            create_contract != str(self.CREATE_USE_EXISTING) and stage == str(STAGE_EDIT)

        self.fields['address'].required = False
        self.fields['rules'].required = False
        self.fields['owner'].required = False

        self.order_fields(['create_contract', 'address', 'rules'])

    def make_ownerfield(self, stage):
        if stage in (str(STAGE_DEPLOY), str(STAGE_STORE)):
            # Need hidden not <select> because at time of loading the list of choices is empty
            self.fields['owner'] = forms.CharField(widget=forms.HiddenInput(),
                                                   label="Owner account",
                                                   help_text=self.fields['owner'].help_text)
        else:
            self.fields['owner'] = forms.ChoiceField(widget=WalletWidget(),
                                                     label="Owner account",
                                                     help_text=self.fields['owner'].help_text)
            if 'owner' in self.data and self.data['owner']:
                self.fields['owner'].choices = [(self.data['owner'], self.data['owner'])]

    def clean(self):
        cleaned_data = super().clean()

    def clean_address(self):
        data = self.cleaned_data['address']
        if self.cleaned_data['create_contract'] == str(self.CREATE_USE_EXISTING) and not data:
            raise forms.ValidationError("Missing value.")
        return data

    def clean_rules(self):
        data = self.cleaned_data['rules']
        if self.cleaned_data['create_contract'] == str(self.CREATE_USE_EXISTING) and not data:
            raise forms.ValidationError("Missing value.")
        return data

    def clean_owner(self):
        data = self.cleaned_data['owner']
        if self.cleaned_data['create_contract'] != str(self.CREATE_NO) and not data:
            raise forms.ValidationError("Missing value.")
        return data

    def clean_symbol(self):
        data = self.cleaned_data['symbol']
        if self.cleaned_data['create_contract'] == str(self.CREATE_USE_EXISTING) and not data:
            raise forms.ValidationError("Missing value.")
        return data

    def clean_name(self):
        data = self.cleaned_data['name']
        if self.cleaned_data['create_contract'] == str(self.CREATE_USE_EXISTING) and not data:
            raise forms.ValidationError("Missing value.")
        return data


# class ProjectContractAddressForm(forms.ModelForm):
#     required_css_class = 'required'
#
#     class Meta:
#         model = ProjectVersion
#         fields = ['address']


class ConfirmProjectForm(forms.Form):
    required_css_class = 'required'

    confirm = forms.BooleanField(
        label="I agree with the terms, the above data is truthful, I am in legal power to agreement")

    def clean_confirm(self):
        if not self.cleaned_data['confirm']:
            raise ValidationError("You have not confirmed the agreement")


class WithdrawalForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Withdrawal
        fields = ['amount', 'purpose', 'wallet']
        widgets = {
            'purpose': forms.Textarea(),
        }

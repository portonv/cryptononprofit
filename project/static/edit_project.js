function on_change_advertisement() {
    var f = $('#id_allow_advertisement').is(":checked");
    $('#id_allow_press_releases').prop('disabled', f);
    if(f) $('#id_allow_press_releases').prop('checked', true);
}

function on_change_ownership() {
    var v = $('[name=ownership]:checked').val();
    $('#id_organization_name').prop('disabled', v != 1 && v != 2);
    $('#id_organization_number').prop('disabled', v != 2);
}

function undim(e) {
    alert(e);
    $('#dimmer').css('display', 'none');
    $('[type=submit]').prop('disabled', false);
}

var abi = undefined, bytecode = undefined;

function do_deploy() {
    var tokenName   = $('#id_contract_name'  ).val();
    var tokenSymbol = $('#id_contract_symbol').val();
    var owner = $('#id_owner').val();
    try {
        var contract = window.web3.eth.contract(abi);
        var contractInstance = contract.new(
            tokenName, tokenSymbol, { data: bytecode, from: owner/*, nonce: block+1*/ },
            function(err, myContract){
                if(err) {
                    undim(err);
                    $('#id_stage').val('0'); // back to editing
                    return;
                }
                // NOTE: The callback will fire twice! https://github.com/ethereum/wiki/wiki/JavaScript-API
                if(myContract && myContract.address) {
                    $('#id_address').val(myContract.address);
                    $('#main_form').submit();
                }
            }
        );
    }
    catch(e) { undim(e); }
}

function deploy() {
    if(!window.web3) return;
    if(!abi || !bytecode) return;
    do_deploy();
}

var wallet_initialized = false;

var saved_wallet_address;
var saved_rules;
var saved_contract_symbol, saved_contract_name;

$(function() {
    saved_wallet_address = $('#id_address').val();
    saved_rules = $('#id_rules').val();
    saved_contract_symbol = $('#id_contract_symbol').val();
    saved_contract_name = $('#id_contract_name').val();
});

function do_create_contract_change_or_set_rules() {
    if($('#id_stage').val() != '0') return;

    var dont_change = $('#id_create_contract_0').is(":checked");
    var create_new = $('#id_create_contract_1').is(":checked");
    var use_existing = $('#id_create_contract_2').is(":checked");

    $('#id_address').prop('disabled', !use_existing);
    $('#id_ethereum_address_fetch').prop('disabled', !use_existing);

    var is_contract = $('#id_rules').val() != "" && $('#id_rules').val() != '-1';

    $('#id_owner').parent().children().css('visibility', !is_contract || dont_change ? 'hidden' : 'visible');
    $('#id_contract_symbol').prop('disabled', !is_contract || dont_change);
    $('#id_contract_name').prop('disabled', !is_contract || dont_change);
    $('#id_owner').prop('required', is_contract && !dont_change);
    $('#id_address').prop('required', !dont_change);
    $('#id_contract_symbol').prop('required', is_contract && !dont_change);
    $('#id_contract_name').prop('required', is_contract && !dont_change);

    if(!wallet_initialized && !dont_change) {
        myweb3_init(init_wallet);
        wallet_initialized = true;
    }
}

function create_contract_change_or_set_rules() {
    do_create_contract_change_or_set_rules();
    do_set_rules();
}

// TODO: Called two times on page load
function create_contract_change() {
    var use_existing = $('#id_create_contract_2').is(":checked");
    if(use_existing) $('#id_rules').val("");

    var create_new = $('#id_create_contract_1').is(":checked");
    if(create_new) {
        $('#id_rules').val('1');
    }
    create_contract_change_or_set_rules();

    var dont_change = $('#id_create_contract_0').is(":checked");
    if(dont_change) {
        $('#id_address').val(saved_wallet_address);
        $('#id_ethereum_address_text').text(saved_wallet_address);
        $('#id_rules').val(saved_rules);
        $('#id_contract_symbol').val(saved_contract_symbol);
        $('#id_contract_name').val(saved_contract_name);
    } else if($('#id_address').val() != "") {
        $('#id_address').val("");
        $('#id_ethereum_address_text').text("");
    }

    if($('#id_create_contract_2').is(":checked") && $('#id_address').val() == "")
        $('[name=show_rules]').css('display', 'none');
}

$(create_contract_change);

function set_rules(type, address) {
    $('#id_rules').val(type);
    $('#id_address').val(address);
    $('#id_ethereum_address_text').text(address);
    do_set_rules();
}

function do_set_rules() {
    var type = $('#id_rules').val();
    $('[name=show_rules]').css('display', 'none');
    if(type != "") {
        $('#show_rules_'+type).css('display', 'block');
        console.log(type, $('#show_rules_'+type).attr('style'));
    }
    do_create_contract_change_or_set_rules();
}

$(do_set_rules);

function change_ethreum_address() {
    var address = "";
    for(;;) {
        address = prompt("Load wallet or contract from address", address);
        if(address === null) return;
        if(web3.isAddress(address)) break; // web3.isChecksumAddress(address)
        alert("Wrong Ethereum address format!");
    }
    fetch_contract(address);
}

function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
}

function contractHasMethod(code, signature) {
    var hash = web3.sha3(encode_utf8(signature)).substring(0, 10);
    return code.indexOf('63' + hash.slice(2, hash.length)) > 0;
}

function ask_rules(address) {
    $('#id_rules_choice_0').prop('checked', true);
    $('#id_ask_rules').dialog({modal: true,
                               buttons: [
                                   {
                                       text: "OK",
                                       click: function() {
                                           set_rules($('[name=rules_choice]:checked').val(), address);
                                           $(this).dialog("close");
                                       }
                                   },
                                   {
                                       text: "Cancel",
                                       click: function() {
                                           $(this).dialog("close");
                                       }
                                   }
                               ]});
}

function fetch_contract(address) {
    $.get('/static/generated/abi.json',
        function(abi) {
            try {
                var MyContract = web3.eth.contract(abi);
                web3.eth.getCode(address, function(error, code) {
                    if(error) {
                        alert(error);
                        return;
                    }
                    try {
                        if(code == '0x') { // not a contract
                            set_rules(-1, address);
                            return;
                        }
                        var myContractInstance = MyContract.at(address);
                        var was_error = false;
                        myContractInstance.symbol.call(function(error, result) {
                            if(was_error) return;
                            if(error) {
                                was_error = true;
                                alert(error);
                                return;
                            }
                            $('#id_contract_symbol').val(result);
                        });
                        myContractInstance.name.call(function(error, result) {
                            if(was_error) return;
                            if(error) {
                                was_error = true;
                                alert(error);
                                return;
                            }
                            $('#id_contract_name').val(result);
                        });
                        if(contractHasMethod(code, 'withdrawEther(address,uint256)')) {
                            ask_rules(address);
                        } else
                            set_rules(0, address);
                    }
                    catch(e) {
                        alert(e);
                    }
                });
            }
            catch(e) {
                alert(e);
            };
        }, 'json');
}

function accomplish_deploy() {
    var next_stage = $('#id_stage').val() == '1' ? '2' : $('#id_create_contract :checked').val() == '1' ? '1' : '2';
    $('#id_stage').val(next_stage);
}
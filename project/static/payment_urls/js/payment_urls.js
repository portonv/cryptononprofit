$(function() {
    // Requires jQuery UI
    $('.urls_editor ul').sortable({
        handle: ".handle"
    });
})

function payment_urls_add(widget_name, url, title) {
    var dummy = $('#id_'+widget_name+'_dummy')[0];
    var elt = $(dummy).clone();
//    elt.css.display = 'block';
    elt.removeAttr('style')
    elt.find('input').prop('required', true);
    elt.find('input[name=payment_url]').val(url);
    elt.find('input[name=payment_title]').val(title);
    $(dummy).parent().append(elt)
}
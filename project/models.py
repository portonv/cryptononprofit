import re

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField
from ordered_model.models import OrderedModel


def ethereum_address_validator(value):
    if value[0:2].lower() != '0x':
        raise ValidationError("Ethereum address must start from 0x")
    if len(value) != 42:
        raise ValidationError("Ethereum address must consist of 42 characters")
    if not re.match(r'^[0-9a-fA-F]', value):
        raise ValidationError("Ethereum address must be hexadecimal")


class PaymentURL(OrderedModel):
    url = models.URLField("URL")
    title = models.CharField("Site title", max_length=255, blank=False)
    project_version = models.ForeignKey('ProjectVersion', on_delete=models.CASCADE, related_name='payment_urls')

    order_with_respect_to = 'project_version'


class EthereumWallet(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    address = models.CharField("Wallet or contract address", max_length=42,
                               validators=[ethereum_address_validator], blank=False)

    class Meta:
        constraints = [
           models.UniqueConstraint(fields=['project', 'address'], name='unique_ethereum_data'),
        ]


TOKEN_RULES = {
    1: "project/contracts/quadratic.html",
}


class ERC20Token(EthereumWallet):
    wallet = models.OneToOneField(EthereumWallet, on_delete=models.CASCADE, primary_key=True)

    contract_symbol = models.CharField("Currency symbol", blank=False, max_length=255,
                                       help_text="Any short string (recommended three capital letters) you may name it")
    contract_name = models.CharField("Currency name", blank=False, max_length=255, help_text="Any name for your new currency")
    owner = models.CharField("Owner account", max_length=42,
                             help_text="The account which controls the contract and on which you may receive money.",
                             validators=[ethereum_address_validator], blank=False)
    rules = models.SmallIntegerField("Contract rules", default=1)


class ProjectImage(models.Model):
    image = models.ImageField(upload_to='project-images/%Y/%m/%d/')


class Project(models.Model):
    disabled = models.BooleanField("Disabled", default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    current_ethereum_wallet = models.ForeignKey('EthereumWallet', verbose_name="Ethereum wallet", null=True,
                                                related_name='erc20_wallets', on_delete=models.SET_NULL)

    @property
    def current_erc20_token(self):
        return getattr(self.current_ethereum_wallet, 'erc20token', None) if self.current_ethereum_wallet else None

    # def equal(self, other):
    #     return self.address == other.address

    def last_version(self):
        return ProjectVersion.objects.filter(project=self).order_by('pk').last()

    def get_absolute_url(self):
        return reverse('project:view', args=[self.pk])


class ProjectVersion(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

    bitcoin_address = models.CharField("BitCoin address for donations (optional)", max_length=35, blank=True, null=True)

    title = models.CharField("Project title", max_length=200, blank=False)
    description = models.TextField("Project description", blank=False)
    money_usage = models.TextField("Allowed use of money", blank=True)
    allow_advertisement = models.BooleanField("Allowed to use money for advertisements")
    allow_press_releases = models.BooleanField(
        "Allowed to use money for press releases (overrides \"allowed to use money for advertisements\")")

    url = models.URLField("Related link", blank=True)

    OWNERSHIP_PERSONAL = 0
    OWNERSHIP_UNREGISTERED = 1
    OWNERSHIP_REGISTERED = 2
    OWNERSHIP_CHOICES = (
        (OWNERSHIP_PERSONAL, 'personal'),
        (OWNERSHIP_UNREGISTERED, 'unregistered nonprofit'),
        (OWNERSHIP_REGISTERED, 'registered nonprofit'),
    )
    ownership = models.SmallIntegerField("Project ownership kind", choices=OWNERSHIP_CHOICES)

    country = CountryField("Country", blank=True)
    organization_name = models.CharField("Organization name", max_length=255, blank=True)
    organization_number = models.CharField("Organization registration number", max_length=20, blank=True)

    project_image = models.ForeignKey('ProjectImage', null=True, on_delete=models.SET_NULL)

    def clean(self):
        if self.allow_advertisement:
            self.allow_press_releases = True
        if self.ownership == ProjectVersion.OWNERSHIP_PERSONAL:
            self.organization_name = ''
        if self.ownership != ProjectVersion.OWNERSHIP_REGISTERED:
            self.organization_number = ''
        if self.organization_number != '' and self.country is None:
            raise ValidationError('If you specify organization number, you must also specify the country.')

    # Should be called after clean()
    def equal(self, other):
        return \
            self.bitcoin_address == other.bitcoin_address and \
            self.title == other.title and \
            self.description == other.description and \
            self.money_usage == other.money_usage and \
            self.allow_advertisement == other.allow_advertisement and \
            self.allow_press_releases == other.allow_press_releases and \
            self.url == other.url and \
            self.ownership == other.ownership and \
            self.country == other.country and \
            self.organization_name == other.organization_name and \
            self.organization_number == other.organization_number and \
            self.project_image == other.project_image and \
            self._equal_payment_urls(other)

    def _equal_payment_urls(self, other):
        # One equality side may be a QuerySet and another list.
        # TODO: No need to retrieve and compare the full lists.
        return list(self.payment_urls_data) == list([{'url': p.url, 'title': p.title} for p in other.payment_urls.all()])

    def get_absolute_url(self):
        return reverse('project:view-old', args=[self.pk])

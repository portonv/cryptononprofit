import mimetypes
from collections import namedtuple
from datetime import datetime

import bleach
import geoip2
import lxml.html
from django.conf import settings
from django.contrib.syndication.views import Feed
from django.db.models import sql
from django.template.response import SimpleTemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.core.paginator import Paginator
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.safestring import mark_safe
from django.views import View
from django.contrib.gis.geoip2 import GeoIP2

from project.base import STAGE_EDIT, STAGE_DEPLOY, STAGE_STORE
from project.forms import ProjectVersionForm, ConfirmProjectForm, \
    ProjectContractSymbolForm, WithdrawalForm
from project.models import Project, ProjectVersion, PaymentURL, ERC20Token, TOKEN_RULES, EthereumWallet, ProjectImage


# hack, no better way in Django 2.2
def _find_field(model_class, attname):
    return next(f for f in ERC20Token._meta.fields if f.attname == attname)


class NeedToShowIntermediaryForm(Exception):
    pass


class CreateOrEdit(LoginRequiredMixin, View):
    def do_contract(self, request, project, symbol_form, now):
        stage = symbol_form.cleaned_data.get('stage')
        create_contract = symbol_form.cleaned_data.get('create_contract')

        if stage in (str(STAGE_EDIT), str(STAGE_DEPLOY)):
            raise NeedToShowIntermediaryForm()

        # stage == STAGE_STORE
        if create_contract in (str(ProjectContractSymbolForm.CREATE_YES),
                               str(ProjectContractSymbolForm.CREATE_USE_EXISTING)):
            wallet = symbol_form.save(commit=False)
            wallet.project = project
            with transaction.atomic():
                # if not wallet.pk:
                #     wallet.wallet = EthereumWallet.objects.create(project=project, address=wallet.address)
                # wallet.full_clean()  # just to be sure of data validity, should have check all data already
                if symbol_form.cleaned_data['rules'] == str(ProjectContractSymbolForm.WALLET_TYPE_REGULAR) and \
                        symbol_form.cleaned_data['create_contract'] == str(
                    ProjectContractSymbolForm.CREATE_USE_EXISTING):
                    if wallet.pk:
                        wallet = wallet.wallet
                        ERC20Token.objects.filter(pk=wallet.pk).delete()  # It is not a token.
                    else:
                        wallet, _ = EthereumWallet.objects.get_or_create(project=project, address=wallet.address)
                    wallet_id = wallet.pk
                else:
                    old_wallet = wallet
                    wallet, _ = EthereumWallet.objects.get_or_create(project=project,
                                                                     address=old_wallet.address,
                                                                     defaults={'created': timezone.now()})
                    if hasattr(wallet, 'erc20token'):
                        wallet = wallet.erc20token
                        wallet.owner = old_wallet.owner
                        wallet.rules = old_wallet.rules
                        wallet.contract_symbol = old_wallet.contract_symbol
                        wallet.contract_name = old_wallet.contract_name
                        wallet.save()
                        wallet_id = wallet.pk
                    else:
                        # Bug https://code.djangoproject.com/ticket/30531
                        # wallet = ERC20Token.objects.create(wallet=wallet,
                        #                                    owner=old_wallet.owner,
                        #                                    rules=old_wallet.rules,
                        #                                    contract_symbol=old_wallet.contract_symbol,
                        #                                    contract_name=old_wallet.contract_name)
                        query = sql.InsertQuery(ERC20Token)
                        MyFields = namedtuple('MyFields',
                                              ['wallet_id',
                                               'owner',
                                               'rules',
                                               'contract_symbol',
                                               'contract_name'])
                        query.insert_values([_find_field(ERC20Token, 'wallet_id'),
                                             _find_field(ERC20Token, 'owner'),
                                             _find_field(ERC20Token, 'rules'),
                                             _find_field(ERC20Token, 'contract_symbol'),
                                             _find_field(ERC20Token, 'contract_name')],
                                            [MyFields(wallet_id=wallet.pk,
                                                      owner=old_wallet.owner,
                                                      rules=old_wallet.rules,
                                                      contract_symbol=old_wallet.contract_symbol,
                                                      contract_name=old_wallet.contract_name)])
                        wallet_id = query.get_compiler(using=ERC20Token.objects.db).execute_sql(return_id=True)

                project.current_ethereum_wallet_id = wallet_id
                project.updated = now
                project.save()

    def update_stage(self, symbol_form, stage, invalid_input):
        symbol_form.data = symbol_form.data.copy()
        symbol_form.data['stage'] = str(stage)
        symbol_form.make_ownerfield(symbol_form.data['stage'])
        if invalid_input:
            symbol_form.fields[
                'owner'].required = False  # Otherwise can't validate because the <select> is initially empty.
        try:
            symbol_form.full_clean()  # update symbol_form.fields['owner'].required message
        except ValueError:
            pass


class Create(CreateOrEdit):
    def get(self, request):
        create_contract = request.GET.get('create_contract')

        form = ProjectVersionForm()
        symbol_form = ProjectContractSymbolForm(create=True, stage=STAGE_EDIT, create_contract=create_contract)
        confirm = ConfirmProjectForm()

        # payment_urls_list = []
        # payment_urls = sortable_helper(request, payment_urls_list)

        return render(request, 'project/edit_project.html',
                      {'form': form,
                       'symbol_form': symbol_form,
                       # 'payment_urls': payment_urls_list,
                       'confirm': confirm,
                       'mode': 'create',
                       'settings': settings})

    def post(self, request):
        stage = request.POST.get('stage')
        create_contract = request.POST.get('create_contract')

        version = ProjectVersion()
        erc20 = ERC20Token()
        form = ProjectVersionForm(request.POST, request.FILES, instance=version)
        symbol_form = ProjectContractSymbolForm(request.POST, create=True,
                                                instance=erc20,
                                                stage=stage, create_contract=create_contract)
        confirm = ConfirmProjectForm(request.POST)
        try:
            form.save(commit=False)
            symbol_form.full_clean()
            # version.full_clean()
            confirm.full_clean()
            symbol_form.full_clean()  # TODO: Why is it called here second time?
        except ValueError as e:
            self.update_stage(symbol_form, STAGE_EDIT, True)
            return render(request, 'project/edit_project.html',
                          {'disabled': False,
                           'form': form,
                           'symbol_form': symbol_form,
                           'confirm': confirm,
                           'mode': 'create'})
        except ValidationError as e:
            # form.add_error(None, e.message_dict[NON_FIELD_ERRORS])
            self.update_stage(symbol_form, STAGE_EDIT, True)
            return render(request, 'project/edit_project.html',
                          {'disabled': False,
                           'form': form,
                           'symbol_form': symbol_form,
                           'confirm': confirm,
                           'mode': 'create'})
        else:
            now = datetime.now(tz=timezone.utc)
            # Transaction because having a project without a project version is inconsistent
            with transaction.atomic():  # TODO: This transaction may contain zero operations.
                project = None
                if request.POST.get('stage') == str(STAGE_STORE):
                    project = Project.objects.create(created=now, updated=now, user=request.user)
                    version.project = project
                    version.save()
                    # Duplicate code.
                    # Bulk creation does not work for OrderedModel.
                    # PaymentURL.objects.bulk_create(
                    #     [PaymentURL(project_version=version, url=p[0], title=p[1])
                    #      for p in version.payment_urls_data])
                    for p in version.payment_urls_data:
                        PaymentURL.objects.create(project_version=version, url=p['url'], title=p['title'])
                try:
                    self.do_contract(request, project, symbol_form, now)
                except (ValueError, NeedToShowIntermediaryForm) as e:
                    if isinstance(e, ValueError):
                        form.add_error(None, str(e))
                    self.update_stage(symbol_form, STAGE_DEPLOY, isinstance(e, ValueError))
                    return render(request, 'project/edit_project.html',
                                  {'disabled': False,
                                   'form': form,
                                   'symbol_form': symbol_form,
                                   'confirm': confirm,
                                   'mode': 'create'})
                return redirect('project:seo', project.pk)


class Edit(CreateOrEdit):
    def get(self, request, pk):
        create_contract = request.GET.get('create_contract')

        version = ProjectVersion.objects.filter(project=pk).order_by('pk').last()
        form = ProjectVersionForm(instance=version)
        confirm = ConfirmProjectForm()
        project = Project.objects.filter(pk=pk).only('disabled').get()
        wallet = project.current_ethereum_wallet
        if hasattr(wallet, 'erc20token'):
            wallet = wallet.erc20token
        symbol_form = ProjectContractSymbolForm(create=False, instance=wallet, stage=STAGE_EDIT,
                                                create_contract=create_contract)
        # project = Project.objects.get(pk=pk)

        # payment_urls_list = PaymentURL.objects.filter(project_version=version.pk)

        return render(request, 'project/edit_project.html',
                      {'project_pk': pk, 'disabled': project.disabled,
                       'wallet': wallet,
                       'erc20': project.current_erc20_token,
                       'symbol_form': symbol_form,
                       'form': form,
                       # 'payment_urls': payment_urls_list,
                       'confirm': confirm,
                       'mode': 'edit'})

    def post(self, request, pk):
        stage = request.POST.get('stage')
        create_contract = request.POST.get('create_contract')

        project = get_object_or_404(Project, pk=pk)
        old_version = ProjectVersion.objects.filter(project=pk).order_by('pk').last()

        # clone old_version
        version = ProjectVersion.objects.get(pk=old_version.pk)
        version.pk = None
        version.project_image = old_version.project_image

        form = ProjectVersionForm(request.POST, request.FILES, instance=version)
        confirm = ConfirmProjectForm(request.POST)

        symbol_form = ProjectContractSymbolForm(request.POST, create=False,
                                                stage=stage, create_contract=create_contract)

        try:
            form.save(commit=False)
            # version.full_clean()
            confirm.full_clean()
            symbol_form.save(commit=False)  # validate
            project = Project.objects.filter(pk=pk).only('disabled').get()
            # project = Project.objects.get(pk=pk)
        except ValueError:
            self.update_stage(symbol_form, STAGE_EDIT, True)
            return render(request, 'project/edit_project.html',
                          {'project_pk': pk,
                           'disabled': project.disabled,
                           'form': form,
                           'symbol_form': symbol_form,
                           'confirm': confirm,
                           'mode': 'edit'})
        except ValidationError as e:
            # form.add_error(None, e.message_dict[NON_FIELD_ERRORS])
            self.update_stage(symbol_form, STAGE_EDIT, True)
            return render(request, 'project/edit_project.html',
                          {'project_pk': pk,
                           'disabled': project.disabled,
                           'form': form,
                           'symbol_form': symbol_form,
                           'confirm': confirm,
                           'mode': 'edit'})

        # Disallow increasing permissions (the HTML fields in question may be disabled):
        if version.allow_advertisement > old_version.allow_advertisement:
            version.allow_advertisement = old_version.allow_advertisement
        if version.allow_press_releases > old_version.allow_press_releases:
            version.allow_press_releases = old_version.allow_press_releases

        with transaction.atomic():
            now = datetime.now(tz=timezone.utc)
            if not version.equal(old_version):  # FIXME: equality condition may change due to a race
                project = get_object_or_404(Project, pk=pk)
                if project.user != request.user:
                    raise ValidationError("Cannot edit project of another user!")
                if request.POST.get('stage') == str(STAGE_STORE):
                    version.created = now
                    version.project = project
                    version.save()
                    # Duplicate code.
                    # Bulk creation does not work for OrderedModel.
                    # PaymentURL.objects.bulk_create(
                    #     [PaymentURL(project_version=version, url=p[0], title=p[1])
                    #      for p in version.payment_urls_data])
                    for p in version.payment_urls_data:
                        PaymentURL.objects.create(project_version=version, url=p['url'], title=p['title'])
            try:
                self.do_contract(request, project, symbol_form, now)
            except (ValueError, NeedToShowIntermediaryForm) as e:
                if isinstance(e, ValueError):
                    form.add_error(None, str(e))
                self.update_stage(symbol_form, STAGE_DEPLOY, isinstance(e, ValueError))
                return render(request, 'project/edit_project.html',
                              {'disabled': False,
                               'form': form,
                               'symbol_form': symbol_form,
                               'confirm': confirm,
                               'mode': 'create'})

        return redirect('project:seo', pk)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class BaseShow(View):
    def do_get(self, request, project, version, there_is_newer=False):
        attrs = bleach.sanitizer.ALLOWED_ATTRIBUTES
        attrs.update({'img': ['src', 'width', 'height']})
        description = mark_safe(bleach.clean(version.description, strip=True,
                                             tags=bleach.sanitizer.ALLOWED_TAGS + ['p', 'pre', 'img'],
                                             attributes=attrs))
        money_usage = mark_safe(bleach.clean(version.money_usage, strip=True,
                                             tags=bleach.sanitizer.ALLOWED_TAGS + ['p', 'pre', 'img'],
                                             attributes=attrs))
        ownership = version.get_ownership_display()
        old = ProjectVersion.objects.filter(project=project.pk).order_by('-pk').only('pk', 'created')
        old_erc20 = ERC20Token.objects.filter(project=project.pk).order_by('-pk')
        rules_text = TOKEN_RULES.get(project.current_erc20_token.rules if project.current_erc20_token else '', '')

        # https://consumer.findlaw.com/securities-law/what-is-the-howey-test.html
        g = GeoIP2()
        try:
            ip = get_client_ip(request)
            if ip:
                country = g.country(ip)['country_code']
            else:
                country = ''  # default city
        except geoip2.errors.AddressNotFoundError:
            country = ''

        return render(request, 'project/show_project.html',
                      {  # 'activity_form': activity_form,
                          'project': project,
                          'version': version,
                          'wallet': project.current_ethereum_wallet,
                          'erc20': project.current_erc20_token,
                          'description': description,
                          'allowed_use': money_usage,
                          'ownership': ownership,
                          'there_is_newer': there_is_newer,
                          'user': request.user,
                          'old': old,
                          'old_erc20': old_erc20,
                          'rules': rules_text,
                          'non_us': country != 'US',
                          'country': country})


class Show(BaseShow):
    def get(self, request, pk):
        project = get_object_or_404(Project, pk=pk)
        version = ProjectVersion.objects.filter(project=pk).order_by('pk').last()
        return self.do_get(request, project, version)


class OldShow(BaseShow):
    def get(self, request, version_pk):
        version = get_object_or_404(ProjectVersion, pk=version_pk)
        project = get_object_or_404(Project, pk=version.project.pk)
        there_is_newer = ProjectVersion.objects.filter(pk__gt=version_pk, project=project.pk)
        return self.do_get(request, project, version, there_is_newer)


class SEOView(View):
    def get(self, request, project_pk):
        return render(request, 'project/seo.html', {'project_pk': project_pk})


class ProjectActivity(LoginRequiredMixin, View):
    def post(self, request, project_pk):
        project = Project.objects.only('user').get(pk=project_pk)
        if project.user != request.user:
            raise ValidationError("Cannot edit project of another user!")
        disabled = request.POST['disabled'] == '1'
        Project.objects.filter(pk=project_pk).update(disabled=disabled)
        return redirect('project:view', project_pk)


class List(View):
    def get(self, request):
        project_list = Project.objects.filter(disabled=False).order_by('-pk')
        if request.GET.get('investment') == '1':
            # code duplication
            project_list = project_list.filter(current_ethereum_wallet__isnull=False).select_related(
                'current_ethereum_wallet__erc20token')

        paginator = Paginator(project_list, 15)
        page = request.GET.get('page', '1')
        projects = paginator.get_page(page)

        versions = []
        for p in projects:
            v = p.last_version()
            # v.short_description = mark_safe(bleach.clean(v.description[:540], strip=True,
            #                                              tags=['i', 'b', 'em', 'strong', 'a']))
            v.short_description = mark_safe(bleach.clean(v.description, strip=True,
                                                         tags=['i', 'b', 'em', 'strong', 'a']))
            v.short_description = mark_safe(bleach.clean(v.short_description[:550], strip=True,
                                                         tags=['i', 'b', 'em', 'strong', 'a']))
            versions.append(v)

        return render(request, 'project/list.html',
                      {'versions': versions,
                       'paginator': paginator,
                       'page_obj': projects,
                       'page': page})


# FIXME: 1. Withdraw from any token of the project. 2. By the way behave sensibly if no token
class Withdraw(LoginRequiredMixin, View):
    def get(self, request, pk):
        project = Project.objects.only('user').get(pk=pk)
        erc20 = project.current_erc20_token  # .only('address', 'owner')
        version = ProjectVersion.objects.filter(project=project).only('title').last()
        form = WithdrawalForm()
        return render(request, 'project/withdraw.html',
                      {'form': form, 'erc20': erc20, 'version': version, 'stage': False})

    def post(self, request, pk):
        project = Project.objects.only('user').get(pk=pk)
        if project.user != request.user:
            raise ValidationError("Cannot withdraw from project of another user!")
        form = WithdrawalForm(request.POST)
        try:
            withdrawal = form.save(commit=False)
        except ValueError:
            erc20 = project.current_erc20_token  # .only('address', 'owner')
            return render(request, 'project/withdraw.html', {'form': form, 'erc20': erc20, 'stage': False})
        if request.POST['done'] != '':
            withdrawal.project = project
            withdrawal.save()
            return redirect('project:seo', pk)
        else:
            erc20 = project.current_erc20_token  # .only('address', 'owner')
            return render(request, 'project/withdraw.html', {'form': form, 'erc20': erc20, 'stage': True})


# AJAX call
class ChangeOwner(LoginRequiredMixin, View):
    def post(self, request, pk):
        token = get_object_or_404(ERC20Token, pk=pk).only('project')
        project = token.project.only('user')
        if project.user != request.user:
            raise ValidationError("Access denied!")

        # if token.owner == request.POST['owner']:
        #     return HttpResponse('')

        ERC20Token.objects.filter(pk=pk).update(owner=request.POST['owner'])

        return HttpResponse('')


# Is `html` binary or string
def _ensure_xml(html):
    if not html or html.isspace():  # lxml.html.fromstring() does not work for empty documents
        return b''
    frag = lxml.html.fromstring(html)
    return lxml.etree.tostring(frag)


class LatestProjectsFeed(Feed):
    feed_url = settings.SITE_URL + '/project/feed'
    title = "New crowdfunding projects - CryptoNGO"
    link = settings.SITE_URL + '/project/list'
    description = "Crowdfunding projects at crypto4ngo.org"
    author_name = "CryptoNGO"
    categories = ["crowdfunding",
                  "BitCoin crowdfunding",
                  "Ether crowdfunding",
                  "Ethereum crowdfunding",
                  "ICO",
                  "Ethereum ICO",
                  "nonprofit",
                  "nonprofits"]

    def items(self):
        return Project.objects.filter(disabled=False).order_by('-created')[:25]

    def item_title(self, item):
        return item.last_version().title

    def item_description(self, item):
        project = get_object_or_404(Project, pk=item.pk)
        version = ProjectVersion.objects.filter(project=item.pk).order_by('pk').last()

        attrs = bleach.sanitizer.ALLOWED_ATTRIBUTES
        attrs.update({'img': ['src', 'width', 'height']})
        description = mark_safe(_ensure_xml(bleach.clean(version.description, strip=True,
                                                         tags=bleach.sanitizer.ALLOWED_TAGS + ['p',
                                                                                               'pre',
                                                                                               'img'],
                                                         attributes=attrs)).decode())
        money_usage = mark_safe(_ensure_xml(
            bleach.clean(version.money_usage, strip=True,
                         tags=bleach.sanitizer.ALLOWED_TAGS + ['p', 'pre', 'img'],
                         attributes=attrs)).decode())
        ownership = version.get_ownership_display()

        response = SimpleTemplateResponse('project/feed_item.html',
                                          {'project': project,
                                           'version': version,
                                           'wallet': project.current_ethereum_wallet,
                                           'erc20': project.current_erc20_token,
                                           'description': description,
                                           'allowed_use': money_usage,
                                           'ownership': ownership,
                                           'settings': settings})
        response.render()
        return response.content.decode('utf-8')

    def item_pubdate(self, item):
        return item.created

    def item_updateddate(self, item):
        return item.updated

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return settings.SITE_URL + reverse('project:view', args=[item.pk])

    def item_enclosure_url(self, item):
        pi = item.last_version().project_image
        return pi and settings.SITE_URL + pi.image.url

    def item_enclosure_length(self, item):
        pi = item.last_version().project_image
        return pi and pi.image.size

    def item_enclosure_mime_type(self, item):
        pi = item.last_version().project_image
        return pi and mimetypes.guess_type(pi.image.name)[0]


class LatestProjectsMainFeed(LatestProjectsFeed):
    pass


class LatestProjectsInvestmentsFeed(LatestProjectsFeed):
    feed_url = settings.SITE_URL + '/project/feed-investments'
    title = "New crowdfunding projects with investments - CryptoNGO"
    link = settings.SITE_URL + '/project/list?investment=1'
    description = "Crowdfunding projects at crypto4ngo.org (only projects with investment/ICO)"

    def items(self):
        project_list = Project.objects.filter(disabled=False).order_by('-created')
        # code duplication
        project_list = project_list.filter(current_ethereum_wallet__isnull=False).select_related(
            'current_ethereum_wallet__erc20token')
        return project_list[:25]

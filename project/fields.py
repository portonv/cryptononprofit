from project.widgets import *


class PaymentURLField(forms.MultiValueField):
    widget = PaymentURLWidget

    def __init__(self, **kwargs):
        # error_messages = {
        #     'incomplete': 'Enter URL and title.',
        # }
        fields = (
            forms.fields.URLField(
                error_messages={'incomplete': 'Enter the payment URL.'},
            ),
            forms.fields.CharField(
                error_messages={'incomplete': 'Enter the site title.'},
            ),
        )
        super().__init__(
            # error_messages=error_messages,
            fields=fields,
            require_all_fields=False,
            **kwargs
        )


class PaymentURLsField(forms.Field):
    widget = PaymentURLsWidget

    def clean(self, value):
        return value
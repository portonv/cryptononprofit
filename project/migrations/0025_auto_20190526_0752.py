# Generated by Django 2.2 on 2019-05-26 07:52

from django.db import migrations, models
import project.models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0024_auto_20190525_2307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='erc20token',
            name='owner',
            field=models.CharField(help_text='The account which controls the contract and on which you may receive money.', max_length=42, validators=[project.models.ethereum_address_validator], verbose_name='Owner account'),
        ),
    ]

from django.db import migrations


def token_wallets(apps, schema_editor):
    EthereumWallet = apps.get_model('project', 'EthereumWallet')
    ERC20Token = apps.get_model('project', 'ERC20Token')
    for erc20 in ERC20Token.objects.all():
        erc20.wallet = EthereumWallet.objects.create(created=erc20.created,
                                                     project=erc20.project,
                                                     address=erc20.contract_address)
        erc20.save()


def current_tokens(apps, schema_editor):
    Project = apps.get_model('project', 'Project')
    for project in Project.objects.all():
        if project.current_erc20_token:
            Project.objects.filter(pk=project.pk).update(current_erc20_wallet=project.current_erc20_token.wallet)


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0027_project_current_erc20_wallet'),
    ]

    operations = [
        migrations.RunPython(token_wallets),
        migrations.RunPython(current_tokens),
    ]

# Generated by Django 2.2 on 2019-05-27 19:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0028_auto_20190527_1941'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='current_erc20_token',
        ),
    ]

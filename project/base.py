STAGE_EDIT = 0  # the form where user edits data
STAGE_DEPLOY = 1  # temporary intermediary (auto-submitted) form which deploys the contract
STAGE_STORE = 2  # store the data in the DB and then redirect

from django import forms


class PaymentURLWidget(forms.MultiWidget):
    template_name = 'project/payment_url_editor.html'

    def __init__(self, attrs):
        widgets = (
            forms.widgets.TextInput(
                attrs=attrs
            ),
            forms.widgets.TextInput(
                attrs=attrs,
            ),
        )
        super().__init__(widgets, attrs)

    # TODO: can remove?
    def decompress(self, value):
        return [value['url'], value['title']]


class PaymentURLsWidget(forms.Widget):
    template_name = 'project/payment_urls_editor.html'

    class Media:
        js = (
            'payment_urls/js/payment_urls.js',
        )
        # Damages footer menu design
        # css = {
        #     'screen': ('https://use.fontawesome.com/releases/v5.8.1/css/all.css',),
        # }

    def render(self, name, value, attrs, renderer):
        return super().render(name, value, attrs, renderer)

    def get_context(self, name, value, attrs):
        d = super().get_context(name, value, attrs)
        d2 = d.copy() if d is not None else []
        attrs2 = attrs.copy() if attrs is not None else {}
        # attrs2['id'] = 'id_' + name + '_dummy'
        attrs2['required'] = False
        d2['empty_value'] = PaymentURLWidget(attrs=attrs2).render('payment', {'url': '', 'title': ''})  # TODO: using fixed name 'payment' is antinatural
        return d2

    def format_value(self, value):
        return value

    def value_from_datadict(self, data, files, name):
        # Remove the first dummy element
        payment_urls = data.getlist('payment_url')[1:]
        payment_titles = data.getlist('payment_title')[1:]
        if len(payment_urls) != len(payment_titles):
            return None

        return [{'url': payment_urls[i], 'title': payment_titles[i]} for i in range(len(payment_urls))]


class WalletWidget(forms.Widget):
    template_name = 'project/widgets/wallet.html'
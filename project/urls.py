from django.urls import path, re_path

import project.views

app_name = 'project'
urlpatterns = [
    path('create', project.views.Create.as_view(), name='create'),
    re_path(r'edit/(\d+)', project.views.Edit.as_view(), name='edit'),
    path(r'seo/<int:project_pk>', project.views.SEOView.as_view(), name='seo'),
    re_path(r'view/(\d+)', project.views.Show.as_view(), name='view'),
    re_path(r'view-old/(\d+)', project.views.OldShow.as_view(), name='view-old'),
    re_path(r'activity/(\d+)', project.views.ProjectActivity.as_view(), name='activity'),
    path('list', project.views.List.as_view(), name='list'),
    path('feed', project.views.LatestProjectsMainFeed(), name='feed'),
    path('feed-investments', project.views.LatestProjectsInvestmentsFeed(), name='feed-investments'),
    re_path(r'withdraw/(\d+)', project.views.Withdraw.as_view(), name='withdraw'),
    # re_path(r'change-owner/(\d+)', project.views.ChangeOwner.as_view(), name='change-owner'),
    re_path(r'change-owner2/(\d+)', project.views.ChangeOwner.as_view(), name='change-owner'),  # make URL different
]

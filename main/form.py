from django.forms import ModelForm

from user.models import Subscriber


class EbookForm(ModelForm):
    class Meta:
        model = Subscriber
        fields = ['email',
                  'fullname',
                  'subscribed',
                  'subscribed_projects',
                  'investor',
                  'donor',
                  'nonprofit']

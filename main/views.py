import bleach
from django.conf import settings
from django.contrib import messages
from django.contrib.sitemaps import Sitemap
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.views.generic import View

from contact.forms import ContactForm
from main.form import EbookForm
from project.models import Project, ProjectVersion
from user.models import User, Subscriber


class MainPage(View):
    def get(self, request):
        contact_form = ContactForm()
        contact_form.fields['from_email'].widget.attrs['class'] = 'form-control'
        contact_form.fields['from_email'].widget.attrs['placeholder'] = 'Email Address*'
        contact_form.fields['subject'].widget.attrs['class'] = 'form-control'
        contact_form.fields['subject'].widget.attrs['placeholder'] = 'Topic*'
        contact_form.fields['text'].widget.attrs['class'] = 'form-control'
        contact_form.fields['text'].widget.attrs['placeholder'] = 'Write Message*'

        projects = Project.objects.filter(disabled=False).order_by('-pk')[:15]
        versions = []
        for p in projects:
            v = p.last_version()
            # v.short_description = mark_safe(bleach.clean(v.description[:540], strip=True,
            #                                              tags=['i', 'b', 'em', 'strong', 'a']))
            v.short_description = mark_safe(bleach.clean(v.description, strip=True,
                                                         tags=['i', 'b', 'em', 'strong', 'a']))
            v.short_description = mark_safe(bleach.clean(v.short_description[:100], strip=True,
                                                         tags=['i', 'b', 'em', 'strong', 'a']))
            versions.append(v)


        return render(request, 'main/mainpage.html', {'contact_form': contact_form,
                                                      'versions': versions})


class CryptoAffiliate(View):
    def get(self, request):
        return render(request, 'main/trade-crypto.html')


class SubscribeView(View):
    def post(self, request):
        try:
            email = request.POST['email']
        except KeyError:
            messages.add_message(request, messages.ERROR, 'Enter email.')
            return redirect('mainpage')
        try:
            obj, created = Subscriber.objects.get_or_create(email=email, subscribed=True, subscribed_projects=True)
        except IntegrityError:
            messages.add_message(request, messages.ERROR, 'Wrong email.')
            return redirect('mainpage')
        if not created:
            obj.subscribed = True
            obj.subscribed_projects = True
            obj.save()
        return redirect('mainpage')


class EbookView(View):
    def get(self, request):
        form = EbookForm(initial={'subscribed': True})
        return render(request, 'main/ebook-form.html', {'form': form})

    def post(self, request):
        form = EbookForm(request.POST)
        form.full_clean()
        if form.is_valid():
            form.save()
            return redirect('https://www.smashwords.com/books/view/947743')
        return render(request, 'main/ebook-form.html', {'form': form})


class MySitemap(Sitemap):
    protocol = settings.PROTOCOL

    # def get_urls(self, site=None, **kwargs):
    #     site = Site(domain=settings.DOMAIN, name=settings.DOMAIN)
    #     return super().get_urls(site=site, **kwargs)


class StaticViewSitemap(MySitemap):
    priority = 1.0

    def items(self):
        return ['mainpage',
                'main:crypto-affiliate', 'main:ebook-form',
                'project:feed',
                'project:feed-investments',
                'docs:raisers',
                'docs:traders',
                'docs:donors',
                'docs:rules',
                'docs:contract',
                'docs:charter',
                'docs:misc',
                'docs:help',
                'docs:privacy',
                'docs:editorial-guidelines',
                'docs:team']

    def location(self, item):
        return reverse(item)


class ProjectSitemap(MySitemap):
    priority = 1.0

    def items(self):
        return Project.objects.filter()

    def lastmod(self, obj):
        return obj.updated


class ProjectVersionSitemap(MySitemap):
    priority = 0.3

    def items(self):
        return ProjectVersion.objects.filter()

    def lastmod(self, obj):
        return obj.created


class UserSitemap(MySitemap):
    priority = 0.5

    def items(self):
        return User.objects.filter()


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class TestView(View):
    def get(self, request):
        return HttpResponse(get_client_ip(request))
from django.urls import path

from main.views import *

app_name = 'main'
urlpatterns = [
    path('crypto-affiliate', CryptoAffiliate.as_view(), name='crypto-affiliate'),
    path('ebook-form', EbookView.as_view(), name='ebook-form'),
    path('subscribe-simple', SubscribeView.as_view(), name='subscribe-simple'),
    path('test', TestView.as_view(), name='test')
]

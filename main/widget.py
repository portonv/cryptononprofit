from django import forms
from django.forms import widgets


class EthereumAddressWidget(forms.TextInput):
    template_name = 'project/widgets/contract_address.html'


class ContractTypeWidget(widgets.Input):
    template_name = 'project/widgets/contract_type.html'

    class Media:
        js = ("https://cdn.jsdelivr.net/gh/vast-engineering/jquery-popup-overlay@2/jquery.popupoverlay.min.js",)

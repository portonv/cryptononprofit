$(document).ajaxError(function( event, request, settings ) {
    alert("Error: " + request.status);
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function ajax_dialog(url, arg) {
    $('#dialog').remove();
    $('body').addClass('wait')
    $.get(url, function(html) {
        $('body').append(html);
        $('#dialog').dialog(arg);
        $('body').removeClass('wait')
    });
    return false;
}

function download_ebook_form(url) {
    return ajax_dialog(url, {
        modal: true,
        buttons: [
            {
                text: "OK",
                click: function () {
                    $(this).dialog("close");
                    $('form', this).submit();
                }
            },
            {
                text: "Cancel",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ],
        width: 600
    });
}

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$('.mean-nav > ul > li > a').click(function () {
    if(!$(event.target).closest('.mean-expand').size())
        $('.meanclose').click();
});

$(function () {
    var selector = '.main-menu li a[href="' + document.location.pathname.replace(/\\/g, "\\\"") + '"]';
    $(selector).closest('li').addClass('active');
})
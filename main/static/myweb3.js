async function myweb3_init(init) {
    var web3js;
    // Modern dapp browsers...
    if (window.ethereum) {
        window.web3 = new Web3(ethereum);
        try {
            // Request account access if needed
            await ethereum.enable();
            // Accounts now exposed
            init();
        } catch (error) {
            // User denied account access...
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);
        // Accounts always exposed
        init();
    }
    // Non-dapp browsers...
    else {
        $('#no_web3_warning').css('display', 'block');
    }
    return web3js;
}

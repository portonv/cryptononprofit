from django.db import models


class Withdrawal(models.Model):
    time = models.DateTimeField("Date and time of withdrawal", auto_now_add=True)
    project = models.ForeignKey('project.Project', verbose_name="Project", on_delete=models.CASCADE)
    # amount = models.DecimalField("Amount (in Ether)", max_digits=78, decimal_places=18)  # may fail with big numbers
    amount = models.CharField("Amount (in Ether)", max_length=79, blank=False)
    purpose = models.TextField("How going to use the money", blank=False)
    wallet = models.CharField(max_length=42, blank=True, help_text="Leave empty to use the owner wallet")

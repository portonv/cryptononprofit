import bleach
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.views import View

from project.models import ProjectVersion
from report.models import Withdrawal


class HistoryView(View):
    def get(self, request, project_pk):
        withdrawals = Withdrawal.objects.filter(project=project_pk).order_by('-pk').defer('wallet').values()
        attrs = bleach.sanitizer.ALLOWED_ATTRIBUTES
        attrs.update({'img': ['src', 'width', 'height']})
        for w in withdrawals:
            w['clean'] = mark_safe(bleach.clean(w['purpose'], strip=True,
                                                tags=bleach.sanitizer.ALLOWED_TAGS + ['p', 'pre', 'img'],
                                                attributes=attrs))
        version = ProjectVersion.objects.filter(project=project_pk).order_by('-pk').last()
        return render(request, 'report/history.html',
                      {'history': withdrawals, 'project_pk': project_pk, 'version': version})

from django.urls import path, re_path

import report.views

app_name = 'report'
urlpatterns = [
    re_path(r'history/(\d+)', report.views.HistoryView.as_view(), name='history'),
]

from django.shortcuts import render
from django.views.generic import View

from docs.misc import render_verbatim


class Raisers(View):
    def get(self, request):
        return render(request, 'docs/raisers.html')


class Traders(View):
    def get(self, request):
        return render(request, 'docs/traders.html')


class Donors(View):
    def get(self, request):
        return render(request, 'docs/donors.html')


class BitcoinEthereum(View):
    def get(self, request):
        return render(request, 'docs/bitcoin-ethereum.html')


class Rules(View):
    def get(self, request):
        return render(request, 'docs/rules.html')


class Contract(View):
    def get(self, request):
        return render(request, 'docs/contract.html')


class ContractSource(View):
    def get(self, request):
        return render_verbatim(request, 'docs/Nonprofit.sol', content_type='text/plain')


class Misc(View):
    def get(self, request):
        return render(request, 'docs/misc.html')


class Charter(View):
    def get(self, request):
        return render(request, 'docs/charter.html')


class Privacy(View):
    def get(self, request):
        return render(request, 'docs/privacy.html')


class EditorialGuidelines(View):
    def get(self, request):
        return render(request, 'docs/editorial-guidelines.html')


class Team(View):
    def get(self, request):
        return render(request, 'docs/team.html')


class Help(View):
    def get(self, request):
        return render(request, 'docs/help.html')

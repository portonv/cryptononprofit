from django.http import HttpResponse
from django.template import loader


def render_verbatim(request, template_name, content_type=None, status=None, using=None):
    content = loader.get_template(template_name, using).template.source
    return HttpResponse(content, content_type, status)
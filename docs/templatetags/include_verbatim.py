from django import template
from django.template import loader
from django.template.loader import get_template
from django.utils.html import escape

register = template.Library()

@register.simple_tag()
def include_verbatim(name):
    """
    Example: {% verbatim_include "weblog/post.html" %}
    """
    template = loader.get_template(name).template.source
    return escape(template)

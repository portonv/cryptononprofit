from django.urls import path

import docs.views

app_name = 'docs'
urlpatterns = [
    path('raisers', docs.views.Raisers.as_view(), name='raisers'),
    path('traders', docs.views.Traders.as_view(), name='traders'),
    path('donors', docs.views.Donors.as_view(), name='donors'),
    path('bitcoin-ethereum', docs.views.BitcoinEthereum.as_view(), name='bitcoin-ethereum'),
    path('rules', docs.views.Rules.as_view(), name='rules'),
    path('contract', docs.views.Contract.as_view(), name='contract'),
    path('charter', docs.views.Charter.as_view(), name='charter'),
    path('misc', docs.views.Misc.as_view(), name='misc'),
    path('help', docs.views.Help.as_view(), name='help'),
    path('privacy-policy', docs.views.Privacy.as_view(), name='privacy'),
    path('editorial-guidelines', docs.views.EditorialGuidelines.as_view(), name='editorial-guidelines'),
    path('team', docs.views.Team.as_view(), name='team'),
]

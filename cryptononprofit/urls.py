"""cryptononprofit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, re_path
from django.contrib import admin

import main.views
from main.views import ProjectSitemap, ProjectVersionSitemap, UserSitemap, StaticViewSitemap
from django.contrib.sitemaps import views as sitemap_views

sitemaps = {'static': StaticViewSitemap,
            'projects': ProjectSitemap,
            'project-versions': ProjectVersionSitemap,
            'users': UserSitemap}

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include(('django.contrib.auth.urls'))),
    path('admin/defender/', include('defender.urls')),
    # path('email/', include('email_change.urls')),
    path('main/', include('main.urls')),
    path('user/', include('user.urls')),
    path('', main.views.MainPage.as_view(), name='mainpage'),
    path('docs/', include('docs.urls')),
    path('contact/', include('contact.urls')),
    path('project/', include('project.urls')),
    path('report/', include('report.urls')),
    path('sitemap.xml', sitemap_views.index, {'sitemaps': sitemaps}),
    path('sitemap-<section>.xml', sitemap_views.sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
